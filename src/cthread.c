#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/support.h"
#include "../include/cthread.h"
#include "../include/cdata.h"

/// Constantes do programa
#define SUCCESS 0   /// função terminou com sucesso
#define FOUND 1     /// função encontrou um elemento
#define ERROR -1    /// função executou incorretamente
#define FAILURE -1  /// o sistema entrou em "pane", em uma situação fatal

#define	PROCST_CRIACAO	0
#define	PROCST_APTO	1
#define	PROCST_EXEC	2
#define	PROCST_BLOQ	3
#define	PROCST_TERMINO	4

int TCBCount = 0;
int main_create = 0;
int system_init = 0;

/// Filas do sistema
FILA2 Ready[4];		/// cada indice 'i' sera a fila de prioridade de indice 'i'
FILA2 Running;
FILA2 Blocked;
FILA2 Waiting;

PFILA2 auxFila;

NODE2 * ptrNode;
NODE2 * ptrRunning;
NODE2 * auxNode;
TCB_t * ptrTCB;
TCB_t * mainTCB;
TCB_t * runningTCB;
TCB_t * checkThread;
TCB_t * prevThread;
TCB_t * auxTCB;


ucontext_t main_context;
ucontext_t * finish_context;
ucontext_t * escalonador_contexto;
ucontext_t * aux_context;
char stack[SIGSTKSZ];
char * ptr_stack;

int removeWaiting(int tid);
int alguemEspera(int tid);

int SystemStart()
{
	/// Criação das filas
	if (CreateFila2(&Ready[0]) == SUCCESS)
	{
		//////printf("Fila Ready 0 criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Ready 0!\n");
		return ERROR;
	}

	if (CreateFila2(&Ready[1]) == SUCCESS)
	{
		//////printf("Fila Ready 1 criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Ready 1!\n");
		return ERROR;
	}

	if (CreateFila2(&Ready[2]) == SUCCESS)
	{
		//////printf("Fila Ready 2 criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Ready 2!\n");
		return ERROR;
	}

	if (CreateFila2(&Ready[3]) == SUCCESS)
	{
		//////printf("Fila Ready 3 criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Ready 3!\n");
		return ERROR;
	}

	if (CreateFila2(&Running) == SUCCESS)
	{
		//////printf("Fila Running criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Running!\n");
		return ERROR;
	}

	if (CreateFila2(&Blocked) == SUCCESS)
	{
		//////printf("Fila Blocked criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Blocked!\n");
		return ERROR;
	}
	if (CreateFila2(&Waiting) == SUCCESS)
	{
		//////printf("Fila Waiting criada com sucesso!\n");
	}
	else
	{
		//////printf("Falha ao criar a Fila Waiting!\n");
		return ERROR;
	}
    system_init = 1;
	return SUCCESS;
}

int escalonador(){
	prevThread = runningTCB;
	//////printf("[ESCALONADOR]\n");
	if(FirstFila2(&Ready[0]) == SUCCESS)
	{
		//////printf("[ESCALONADOR] FILA 0\n");
		ptrNode = GetAtIteratorFila2(&Ready[0]);
		DeleteAtIteratorFila2(&Ready[0]);
		ptrTCB = ptrNode->node;
		ptrTCB->state = PROCST_EXEC;
		runningTCB = ptrTCB;
		setcontext(&runningTCB->context);
	}
	else if(FirstFila2(&Ready[1]) == SUCCESS)
	{
		//////printf("[ESCALONADOR] FILA 1\n");
		FirstFila2(&Ready[1]);
		ptrNode = GetAtIteratorFila2(&Ready[1]);
		DeleteAtIteratorFila2(&Ready[1]);
		ptrTCB = ptrNode->node;
		ptrTCB->state = PROCST_EXEC;
		runningTCB = ptrTCB;
		//swapcontext()
		setcontext(&runningTCB->context);
		}
	else if(FirstFila2(&Ready[2]) == SUCCESS)
	{
		//////printf("[ESCALONADOR] FILA 2\n");
		ptrNode = GetAtIteratorFila2(&Ready[2]);
		DeleteAtIteratorFila2(&Ready[2]);
		ptrTCB = ptrNode->node;
		ptrTCB->state = PROCST_EXEC;
		runningTCB = ptrTCB;
		setcontext(&runningTCB->context);
	}
	else if(FirstFila2(&Ready[3]) == SUCCESS)
	{
		//////printf("[ESCALONADOR] FILA 3\n");
		ptrNode = GetAtIteratorFila2(&Ready[3]);
		DeleteAtIteratorFila2(&Ready[3]);
		ptrTCB = ptrNode->node;
		ptrTCB->state = PROCST_EXEC;
		runningTCB = ptrTCB;
		setcontext(&runningTCB->context);
	}

	return SUCCESS;
}

int criarMain()
{
	mainTCB = (TCB_t*) malloc(sizeof(TCB_t));
	if(mainTCB == NULL)
	{
		//////printf("Erro ao criar Main Thread!\n");
		return ERROR;
	}

	ptrTCB = malloc(sizeof(TCB_t));
	mainTCB->tid = TCBCount;
	mainTCB->state = PROCST_EXEC;
	mainTCB->ticket = 0;

	getcontext(&(mainTCB->context));

	mainTCB->context.uc_stack.ss_sp = stack;
	mainTCB->context.uc_stack.ss_size = sizeof(stack);
	mainTCB->context.uc_stack.ss_flags = 0;	///precisa?
	mainTCB->context.uc_link = NULL;

	runningTCB = mainTCB;

	ptrTCB = mainTCB;
	TCBCount++;

	//////printf("Thread Main criada com sucesso! Sua TID e': %d\n", mainTCB->tid);

	main_create = 1;

	escalonador_contexto = malloc(sizeof(ucontext_t));
	getcontext(escalonador_contexto);
	escalonador_contexto->uc_stack.ss_sp = (char *) malloc(SIGSTKSZ);
	escalonador_contexto->uc_stack.ss_size = SIGSTKSZ;
	escalonador_contexto->uc_link = NULL;
	makecontext(escalonador_contexto,(void (*)(void)) escalonador,0);

	return SUCCESS;
}


void cfinish(int tid){
	int i;
    //////printf("Cfinish is running! TID da Thread bloqueada: %d \n", tid);
    if(tid > -1){
	    FirstFila2(&Blocked);
			ptrNode = GetAtIteratorFila2(&Blocked);
	    checkThread = ptrNode->node;
	    while(checkThread != NULL){
				if(checkThread->tid == tid){
					//////printf("[CFINISH] TID encontrado(tid=%d).\n",checkThread->tid);
					DeleteAtIteratorFila2(&Blocked);
					checkThread->state = PROCST_APTO;
					ptrNode->node = checkThread;
					i = checkThread->ticket;
					AppendFila2(&Ready[i],ptrNode);
					removeWaiting(runningTCB->tid);
					//////printf("[CFINISH] Finished.\n");
					runningTCB->state = PROCST_TERMINO;

					swapcontext(&runningTCB->context, escalonador_contexto);
					return;
				}
				//////printf("[CFINISH] PID ~ainda~ não encontrado. TID: %d - TID THREAD: %d\n",tid, checkThread->tid);
				NextFila2(&Blocked);
				checkThread = (TCB_t*)GetAtIteratorFila2(&Blocked);
	  	}
	    FirstFila2(&Blocked);
	    //////printf("[CFINISH] PID não existe.\n");
	    return;
    }

    runningTCB->state = PROCST_TERMINO;
		//////printf("[THREAD ID: %d FINALIZADA]\n",runningTCB->tid);

		swapcontext(&runningTCB->context, escalonador_contexto);
}

int criarFinishContext()
{
	finish_context = (ucontext_t*)malloc(sizeof(ucontext_t));
	if(finish_context == NULL)
	{
		//////printf("Erro ao criar Finish Context!\n");
	}

	finish_context->uc_stack.ss_sp = (char *) malloc(SIGSTKSZ);
	finish_context->uc_stack.ss_size = SIGSTKSZ;
	finish_context->uc_link = NULL;
	getcontext(finish_context);
	makecontext(finish_context,(void (*)(void))cfinish,1,-1);

	return SUCCESS;
}


int ccreate (void* (*start)(void*), void *arg, int prio)
{
	if (system_init == 0)
	{
		SystemStart();
	}
	///A primeira chamada das funçoes da thread deve criar o contexto pra main
	if (main_create == 0)
	{
		criarMain();
	}
	criarFinishContext();
	ptrTCB = malloc(sizeof(TCB_t));
	ptrTCB->tid = TCBCount;
	ptrTCB->state = PROCST_APTO;
	ptrTCB->ticket = prio;
	getcontext(&ptrTCB->context);
	ptrNode = malloc(sizeof(NODE2));
	ptrNode->node = ptrTCB;
	ptrTCB->context.uc_link = finish_context;
	ptrTCB->context.uc_stack.ss_sp = (char *) malloc(SIGSTKSZ);
	ptrTCB->context.uc_stack.ss_size = SIGSTKSZ;
	makecontext(&(ptrTCB->context), (void (*)(void))start, 1, (void *)arg);
	LastFila2(&Ready[prio]);
	AppendFila2(&Ready[prio], ptrNode);
	TCBCount++;
	//////printf("Criou uma thread! Sua TID e': %d  Prioridade: %d\n", ptrTCB->tid, ptrTCB->ticket);

	return ptrTCB->tid;
}

int cyield(void){
	//////printf("[CYIELD]\n");

	ptrNode = malloc(sizeof(NODE2));
	ptrNode->node = runningTCB;

	LastFila2(&Ready[runningTCB->ticket]);
	AppendFila2(&Ready[runningTCB->ticket],ptrNode);

	swapcontext(&runningTCB->context,escalonador_contexto);
	//////printf("[CYIELD] Finish\n");

	return SUCCESS;

}

int csetprio(int tid, int prio)
{
	int hasTCB = 0;
	int i;

	if (prio < 0 && prio > 4)
	{
		//////printf("[CSETPRIO] ERRO: Prioridade invalida para o sistema!\n");
		return ERROR;
	}

	for (i = 0; i < 4; i++)	// para cada fila Ready do sistema ...
	{
		if(FirstFila2(&Ready[i]) == SUCCESS)	// ... se há algum elemento na fila
		{
			//////printf("\n[CSETPRIO]: Estou na fila %d\n", i);

			while(hasTCB == SUCCESS)	// ... enquanto tiver elementos na fila
			{
				auxNode = GetAtIteratorFila2(&Ready[i]);	// ... pega o elemento apontado pelo iterador
				auxTCB = auxNode->node;

				//////printf("\nAAAAAAA\n");
				if(auxTCB != NULL)	// ... se o nodo atual do iterador não for nulo
				{
					if(auxTCB->tid == tid && auxTCB->ticket != prio)	// ...se o tid do nodo atual é igual à tid recebida por parâmetro, e se a nova prio for diferente à prio do nodo, fazer as alterações
					{
						//////printf("\n[CSETPRIO]: Encontrei a Thread %d!\n", tid);
						DeleteAtIteratorFila2(&Ready[i]);	// deleta da i-ésima fila
						auxTCB->ticket = prio;

						auxNode->node = auxTCB;
						LastFila2(&Ready[prio]);
						AppendFila2(&Ready[prio], auxNode);	// insere na fila correspondente (por que nao consigo com 'prio' e consigo com 'i'??
						return SUCCESS;
					}
				}
				hasTCB = NextFila2(&Ready[i]);	// vai para o próximo elemento da fila
			}
		}
	}

	//////printf("\n[CSETPRIO] Nao encontrei a thread %d!\n", tid);
	return ERROR;
}

int cjoin(int tid){
	//////printf("[CJOIN] TID: %d\n",tid);
	int i = 0;
		for(i = 0; i < 4; i++)
		{
			if(FirstFila2(&Ready[i])==SUCCESS)
			{
				//////printf("[CJOIN] VARRENDO FILA %d\n",i );
				ptrNode = GetAtIteratorFila2(&Ready[i]);
				checkThread = ptrNode->node;
				while(checkThread != NULL){
					if(checkThread->tid == tid){
						//////printf("[CJOIN] TID encontrado(tid=%d).\n",checkThread->tid);
						if(alguemEspera(checkThread->tid) == 1){
							//////printf("[CJOIN] TID já está sendo esperado.\n");
							FirstFila2(&Ready[i]);
							return -1;
						}
						makecontext(checkThread->context.uc_link,(void (*)(void)) cfinish,1, runningTCB->tid);

						//////printf("TID DA THREAD LINKADA: %d\n", checkThread->tid);

						LastFila2(&Blocked);

						ptrRunning = malloc(sizeof(NODE2));
						ptrRunning->node = runningTCB;
						AppendFila2(&Blocked,ptrRunning);

						LastFila2(&Waiting);
						AppendFila2(&Waiting, ptrNode);

						swapcontext(&runningTCB->context,escalonador_contexto);
						//////printf("[CJOIN] Finished.\n");
						return SUCCESS;
					}
					//////printf("[CJOIN] PID ~ainda~ não encontrado.\n");
					//////printf("[CJOIN] TID CHECK: %d TID: %d\n",checkThread->tid,tid );
					if(NextFila2(&Ready[i]) != SUCCESS){
						//////printf("[CJOIN] PID não encontrado!\n");
						//return SUCCESS;
						break;
					}
					ptrNode = GetAtIteratorFila2(&Ready[i]);
					checkThread = ptrNode->node;
			  	}
			}
		}

    //////printf("[CJOIN] PID não existe.\n");
    return -2;
}

/// FUNÇÕES DE SEMÁFORO

 int csem_init (csem_t *sem, int count)
 {
 	sem->count = count;
 	int wasQueueCreated = CreateFila2((PFILA2)&sem->fila);

 	if (sem->count == count && wasQueueCreated == SUCCESS)
 	{
 		/// DEBUG
 		//printf("\n[CSEM_INIT] Semaforo inicializado com sucesso! Seu count e': %d\n", sem->count);
 		return SUCCESS;
 	}
 	else
 	{
 		return ERROR;
 	}
 }
 
//FILA2 Ready[4];
/*
typedef struct s_sem {
	int	count;	// indica se recurso est� ocupado ou n�o (livre > 0, ocupado = 0)
	PFILA2	fila; 	// ponteiro para uma fila de threads bloqueadas no sem�foro
} csem_t;
*/
/*
CWAIT:
P(s): s.valor = s.valor - 1;
se s.valor < 0 {
Insere processo P em s.fila
Bloqueia processo P (sleep);
}*/
int cwait(csem_t *sem)
{
	sem->count--;
	//printf("\n[CWAIT] Estou antes do if!\n");
	if (sem->count <= 0)		// inserir na fila do semáforo e o bloqueia
	{
		//printf("\n[CWAIT] Estou dentro do if!\n");
		AppendFila2((PFILA2)&sem->fila, ptrRunning);
	}
	
	return SUCCESS;
}

/*
CSIGNAL:
V(s): s.valor = s.valor + 1
se s.valor <=0 {
Retira um processo P ́de s.fila;
Acorda processo P ́ (wakeup);
}*/
int csignal(csem_t *sem)
{
	//printf("\n[CSIGNAL] Estou antes do incremento!\n");
	sem->count++;
	
	//printf("\n[CSIGNAL] Estou antes do if!\n");
	if (sem->count > 0)	// retirar da fila e acordar P
	{
		//printf("AAAAAAAAAA");
		FirstFila2((PFILA2)&sem->fila);
		//printf("BBBBBBBBBBB");
		auxNode = GetAtIteratorFila2((PFILA2)&sem->fila);
		//printf("CCCCCCCCCCC");
		DeleteAtIteratorFila2((PFILA2)&sem->fila);
		//printf("DDDDDDDDDDD");
		auxTCB = auxNode->node;
		//printf("EWEEEEEEE");
		AppendFila2((PFILA2)&Ready[auxTCB->ticket], auxNode);
		//printf("FFFFFFFFFFFFf");
	}
	
	return SUCCESS;
}

int cidentify (char *name, int size)
{
	if (size >= 49)
	{
		puts(name);
		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int alguemEspera(int tid){
	if(FirstFila2(&Waiting) != SUCCESS)
		return SUCCESS;
	NODE2 * Node_aux;
	TCB_t * TCB_aux;
	Node_aux = GetAtIteratorFila2(&Waiting);
	while (Node_aux != NULL) {
		TCB_aux = Node_aux->node;
		if (TCB_aux->tid == tid){
			//////printf("TID %d ESTA NA FILA DE ESPERA!\n", tid);
			return 1;
		}
		NextFila2(&Waiting);
		Node_aux = GetAtIteratorFila2(&Waiting);
	}

	return SUCCESS;

}

int removeWaiting(int tid){
	if(FirstFila2(&Waiting) != SUCCESS)
		return SUCCESS;
	NODE2 * Node_aux;
	TCB_t * TCB_aux;
	Node_aux = GetAtIteratorFila2(&Waiting);
	while (Node_aux != NULL) {
		TCB_aux = Node_aux->node;
		if (TCB_aux->tid == tid){
			DeleteAtIteratorFila2(&Waiting);
			//////printf("[TID %d RETIRADO DA FILA DE WAITING]\n", tid);
		}
		NextFila2(&Waiting);
		Node_aux = GetAtIteratorFila2(&Waiting);
	}
	return SUCCESS;
}
