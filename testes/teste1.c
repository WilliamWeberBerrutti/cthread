/**
*	Programa de testes para o Trabalho 1 de SISOP I - 2017/1
* 
* Grupo: Joao Gubert e William W. Berrutti
* Objetivo do teste: testar chamadas 'cidentify()', 'ccreate()', 'cjoin()'
**/

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/support.h"
#include "../include/cthread.h"
#include "../include/cdata.h"
#include "../src/cthread.c"

#define SIZE 100



void* f(void * a)
{	
	int result = 1;
    int i;
    
    for (i = 1; i <= SIZE; i++)
    {
        result+=10*i;
    }

    printf("\nRESULT IS: %d", result);	// resultado esperado deve ser 100 para cada thread
    return 0;
    /*int i, tmp;
	printf("\nCWAIT AQUI\n");
    cwait((void*)&sem);	/// início da seção crítica
    cyield();	// para a thread ceder voluntariamente a execução, e testar o funcionamento dos semáforos
    for(i = 0; i < NITER; i++)
    {
        tmp = cnt;
        tmp = tmp+1;
        cnt = tmp;
    }
	printf("\nCSIGNAL AQUI\n");
    csignal((void*)&sem);	/// final da seção crítica
    return NULL;*/
}

/*void* f1 ()
{
    int result = 1;
    int i;
    
    for (i = 1; i <= SIZE; i++)
    {
        result*=i;
    }

    printf("\nRESULT IS: %d", result);	// resultado esperado deve ser 100 para cada thread
    return 0;
}*/

int main(int argc, char *argv[])
{
	int	id0, id1;
	//int i;
	int arg = 1;
    
	printf("\n\n ===== Identificando a dupla do trabalho =====\n\n");
	cidentify("Joao Gubert - 273166\nWilliam W. Berrutti - 205693", 49);
	
	printf("\n\n ===== Criando as threads =====\n\n");
	id0 = ccreate((void *)f,(void *)arg, 1);
	id1 = ccreate((void *)f,(void *)-arg, 2);

	printf("\n\n ===== Passando a execucao para thread %d =====\n\n", id0);
	cjoin(id0);
	printf("\n\n ===== Passando a execucao para thread %d =====\n\n", id1);
	cjoin(id1);
	
	return SUCCESS;
}
