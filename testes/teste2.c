/**
* 	Programa de testes para o Trabalho 1 de SISOP I - 2017/1
* 
* Grupo: Joao Gubert e William W. Berrutti
* Objetivo do teste: testar chamadas 'csem_init()', 'ccreate()', 'cjoin()', 'cwait()', 'cyield()', 'csignal()'
**/

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/support.h"
#include "../include/cthread.h"
#include "../include/cdata.h"
#include "../src/cthread.c"

#define NITER 1000000

csem_t sem;

int cnt = 0;	// 'cnt' deve ter como resultado 4*NITER, pelo que entendi

void* Count(void * a)
{	
    int i, tmp;
	printf("\nCWAIT AQUI\n");
    cwait(&sem);	/// início da seção crítica
    cyield();	// para a thread ceder voluntariamente a execução, e testar o funcionamento dos semáforos
    for(i = 0; i < NITER; i++)
    {
        tmp = cnt;
        tmp = tmp+1;
        cnt = tmp;
    }
	printf("\nCSIGNAL AQUI\n");
    csignal(&sem);	/// final da seção crítica
    return NULL;
}


int main(){
    int arg = 10;
    int t1, t2, t3, t4;
    
	printf("\n========= CRIACAO SEMAFORO ==========\n");
	csem_init(&sem, 1);
	
	t1 = ccreate((void *)Count,(void *)arg, 0);
	t2 = ccreate((void *)Count,(void *)arg, 0);
	t3 = ccreate((void *)Count,(void *)arg, 0);
	t4 = ccreate((void *)Count,(void *)arg, 0);
	
    //cyield();
    
    cjoin(t1);
    cjoin(t2);
    cjoin(t3);
    cjoin(t4);
    
    printf("Thread criada com sucesso!\n");
	printf("\nCNT is: %d\n", cnt);

	
    return 0;
}

