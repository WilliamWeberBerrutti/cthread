/**
* 	Programa de testes para o Trabalho 1 de SISOP I - 2017/1 (baseado num programa de exemplo do professor)
* 
* Grupo: Joao Gubert e William W. Berrutti
* Objetivo do teste: testar chamadas 'csem_init()', 'ccreate()', 'csetprio()', 'cjoin()'
**/

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/support.h"
#include "../include/cthread.h"
#include "../include/cdata.h"
#include "../src/cthread.c"

csem_t* mutex;

void* fatorial(void *i)
{
	int fat=1, n, number;

	number = n = *(int *)i;
	printf("\nSou a thread Fatorial!\n");
	cyield();
	for (; n > 1; --n)
		fat = n * fat;

	printf("Fatorial de %d e': %d\n\n", number, fat);
	return 0;
}

void* fibonnaci (void *i)
{
	int fi, fj, fk, k, n;

	n = *(int *)i;

	fi = 0;
	fj = 1 ;
	printf("\nSou a thread Fibonacci!\n");
	printf ("0 1");
	for (k = 1; k <= n; k++)
	{
		fk = fi + fj;
		fi = fj;
		fj = fk;
		printf(" %d", fk);
	}

	printf("\n");
	return 0;
}

int main(int argc, char **argv)
{
	int id0, id1;
	int i = 4;
	int newprio = 0;
	int idToChange;

	csem_init((void*)&mutex, 0);

	id0 = ccreate(fatorial, (void *)&i,1);
	idToChange = id1 = ccreate(fibonnaci, (void *)&i,1);

    printf("\n ===== Trocando a prioridade da Thread %d =====\n", idToChange);

	if (csetprio(idToChange, newprio) == SUCCESS)
	{
		printf("\n ===== Thread %d teve sua prioridade alterada para %d! =====\n", idToChange, newprio);
	}
	else
		printf("\n ===== Falha ao alterar a prioridade! =====\n");

	cjoin(id0);
	cjoin(id1);

	return SUCCESS;
}
