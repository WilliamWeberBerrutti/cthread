
/*
 *	Programa de exemplo de uso da biblioteca cthread
 *
 *	Vers�o 1.0 - 14/04/2016
 *
 *	Sistemas Operacionais I - www.inf.ufrgs.br
 *
 */

#include "../include/support.h"
#include "../include/cthread.h"
#include <stdio.h>

void* func0(void *arg) {
	printf("Eu sou a thread ID0 imprimindo %d\n", *((int *)arg));
	cyield();
	printf("final ID0\n");
	return;
}

void* func1(void *arg) {
	printf("Eu sou a thread ID1 imprimindo %d\n", *((int *)arg));
}

int main(int argc, char *argv[]) {

	int	id0, id1, id2;
	int i = 12;

	id0 = ccreate(func0, (void *)&i,1);
	id1 = ccreate(func1, (void *)&i,1);
	id2 = ccreate(func1, (void *)&i,1);

	printf("Eu sou a main apos a criaco de ID0 e ID1\n");

	//cyield();

	cjoin(id0);
	printf("Após finalizar thread ID0!\n" );
	cjoin(id1);

	printf("Eu sou a main voltando para terminar o programa\n");
}
