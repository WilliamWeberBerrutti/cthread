CC=gcc
LIB_DIR=./lib
INC_DIR=./include
BIN_DIR=./bin
SRC_DIR=./src

all: cthread.o libcthread.a

libcthread.a: cthread.o
	ar crs libcthread.a $(BIN_DIR)/cthread.o $(BIN_DIR)/support.o
	mv libcthread.a $(LIB_DIR)

cthread.o:
	$(CC) -c $(SRC_DIR)/cthread.c -Wall
	mv cthread.o $(BIN_DIR)

clean:
	rm -r $(BIN_DIR)/cthread.o $(LIB_DIR)/libcthread.a


